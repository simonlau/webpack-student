"use strict";

require("../styles/style.scss");
const msg = require("./content.js");
const $ = require("jquery");
const angular = require("angular");

$(document).ready(() => {
    $('#content').text("ES7: " + msg.helloMsg);
});

angular.module('app', []);
